#!/bin/bash

. ./../secrets_to_env.sh

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/28384181/terraform/state/default" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/28384181/terraform/state/default/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/28384181/terraform/state/default/lock" \
    -backend-config="username=peterunger" \
    -backend-config="password=${GITLAB_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"