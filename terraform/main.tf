variable "hcloud_token" {
  type = string
}
variable "hcloud_location" {
  type = string
  default = "hel1"
}
variable "ssh_public_key" {
  type = string
}

terraform {
  backend "http" {
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

data "hcloud_location" "location" {
  name = var.hcloud_location
}

resource "hcloud_ssh_key" "infrastructure" {
  name       = "Infrastructure"
  public_key = var.ssh_public_key
}

resource "hcloud_server" "server" {
  count       = 1
  name        = "server-${count.index}"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location    = data.hcloud_location.location.name
  ssh_keys    = [hcloud_ssh_key.infrastructure.id]
  labels = {
    "unger.xyz/server" = "true",
    "unger.xyz/agent"  = "false"
  }
  firewall_ids = [hcloud_firewall.base.id, hcloud_firewall.k3s-server.id]
}

resource "hcloud_server" "agent-cx21" {
  count       = 2
  name        = "agent-cx21-${count.index}"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location    = data.hcloud_location.location.name
  ssh_keys    = [hcloud_ssh_key.infrastructure.id]
  labels = {
    "unger.xyz/server" = "false",
    "unger.xyz/agent"  = "true"
  }
  firewall_ids = [hcloud_firewall.base.id]
}

resource "hcloud_volume" "k3s-data" {
  name = "k3s-data"
  location = data.hcloud_location.location.name
  size = 50
  format = "xfs"
}

resource "hcloud_volume_attachment" "k3s-data" {
  volume_id = hcloud_volume.k3s-data.id
  server_id = hcloud_server.server[0].id
  automount = true
}

resource "hcloud_network" "k3s" {
  name     = "k3s"
  ip_range = "10.29.0.0/16"
}

resource "hcloud_network_subnet" "k3s" {
  network_id   = hcloud_network.k3s.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "10.29.0.0/24"
}

resource "hcloud_server_network" "server" {
  count      = length(hcloud_server.server)
  server_id  = hcloud_server.server[count.index].id
  network_id = hcloud_network.k3s.id
}

resource "hcloud_server_network" "agent-cx21" {
  count      = length(hcloud_server.agent-cx21)
  server_id  = hcloud_server.agent-cx21[count.index].id
  network_id = hcloud_network.k3s.id
}

resource "hcloud_firewall" "base" {
  name = "base"
  rule {
    direction  = "in"
    protocol   = "icmp"
    source_ips = ["0.0.0.0/0", "::/0"]
  }
  rule {
    direction  = "in"
    protocol   = "tcp"
    port       = "22"
    source_ips = ["0.0.0.0/0", "::/0"]
  }
}

resource "hcloud_firewall" "k3s-server" {
  name = "k3s-server"
  rule {
    direction  = "in"
    protocol   = "tcp"
    port       = "6443"
    source_ips = ["0.0.0.0/0", "::/0"]
  }
}