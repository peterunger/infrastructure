#!/bin/bash

DIR="$( cd "$( dirname "$0" )" && pwd )"
echo "$(cd $DIR && kubectl kustomize && ./gen_secrets.sh)" | kubectl apply -f -
