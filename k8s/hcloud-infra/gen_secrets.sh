#!/bin/bash

#export HCLOUD_TOKEN=$(gopass infrastructure/hcloud_token)
export NETWORK_NAME="k3s"

echo "---"
kubectl -n kube-system create secret generic hcloud \
  --from-literal=token=$HCLOUD_TOKEN \
  --from-literal=network=$NETWORK_NAME \
  --dry-run -o yaml