#!/bin/bash

export INFRA_SECRETS_DIR="$(git rev-parse --show-toplevel)/.secrets"
export GITLAB_TOKEN=$(gopass infrastructure/gitlab-token)
export HCLOUD_TOKEN=$(gopass infrastructure/hcloud_token)

export TF_VAR_hcloud_token=$HCLOUD_TOKEN
export TF_VAR_public_key_file="$INFRA_SECRETS_DIR/ssh/infrastructure.pub"