#!/bin/bash

. "$(git rev-parse --show-toplevel)/secrets_to_env.sh"

SECRETS_DIR=$INFRA_SECRETS_DIR
mkdir -p "$SECRETS_DIR"

echo "--- Get infrastructure ssh keypair ---"
mkdir -p "$SECRETS_DIR/ssh"
gopass infrastructure/ssh_pubkey | base64 -d > "$SECRETS_DIR/ssh/infrastructure.pub"
chmod 400 "$SECRETS_DIR/ssh/infrastructure.pub"
gopass infrastructure/ssh_privkey | base64 -d > "$SECRETS_DIR/ssh/infrastructure"
chmod 400 "$SECRETS_DIR/ssh/infrastructure"