#!/bin/bash

. ./../secrets_to_env.sh

pip install -r requirements.txt
ansible-galaxy install -r requirements.yml